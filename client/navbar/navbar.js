Template.navbar.events({
    'click #newPlan': function () {
        $('#newPlan').addClass('active');
        $('#myPlan').removeClass('active');
        $('#setting').removeClass('active');
    },

    'click #myPlan': function () {
        $('#myPlan').addClass('active');
        $('#newPlan').removeClass('active');
        $('#setting').removeClass('active');
    },

    'click #setting': function () {
        $('#newPlan').removeClass('active');
        $('#myPlan').removeClass('active');
        $('#setting').addClass('active');
    }


});