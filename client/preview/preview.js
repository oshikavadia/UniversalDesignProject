Template.preview.rendered = (function () {
    canvas = document.getElementById('floorPlan');
    context = canvas.getContext('2d');
    context.fillStyle = '#ecf7ff';
    context.fillRect(0, 0, 1200, 800);


    // console.log("hahaha"+Session.get("venueInfo"));

    var venue = JSON.parse(Session.get("venueInfo"));
    // console.log("thigy haha"+ venue.row);
    var capacity = parseInt(venue.capacity);
    var col = parseInt(venue.column);
    var row = parseInt(venue.row);


    function drawRect(g, capacity, w, h) {
        /* Draw on the canvas */
        g.strokeStyle = "#2005ba";
        g.strokeRect(0, 0, w, h);
    }


    for (var j = 0; j <= col; j++) {
        for (var i = 1; i <= row; i++) {
            drawRect(context, capacity, ((1200 / row) * i), j * (800 / col));
        }
    }

});