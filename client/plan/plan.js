/*
 Seat number - Name - Exam Subject - signature - Comments
 */
 
Template.plan.events({
    'click .add': function (e) {
        var $this = $(e.target);
        e.preventDefault();
        $this.attr('src', "/images/minusButton.png");
        $this.removeClass("add");
        $this.addClass("minus");
		
		// alert((e.currentTarget.alt).substr(0,e.currentTarget.alt.indexOf(" ")));

		var index=parseInt((e.currentTarget.alt).substr(e.currentTarget.alt.indexOf(" ")));
		//console.log(index);
		//console.log("a");
		console.log(index);
		var temp=JSON.parse(Session.get("students"))[index].course;
		//	console.log("b");

		var id_t=(e.currentTarget.alt).substr(0,e.currentTarget.alt.indexOf(" "));

		var studentsToPut;
		if (id_t.length!=5){
		
			studentsToPut= temp.reduce(function(a, b) { 
					return a.concat(b.students);
			}, []);
		} else {
			studentsToPut=temp.filter(function(a){
				if (a.prog.courseID==id_t){
					return true;
				} else {
					return false;
				}
			})[0].students;
			// alert(studentsToPut.length);
			console.log(studentsToPut);
		}
		
		


 


        canvas = document.getElementById('floorPlan');
        g = canvas.getContext('2d');


        var venue = JSON.parse(Session.get("venueInfo"));
        var capacity = parseInt(venue.capacity);
        var col = parseInt(venue.column);
        var row = parseInt(venue.row);

		function fillBox(g, posX,posY, w, h,color){
			g.fillStyle = color;
			g.fillRect(posX,posY,w,h);
			g.strokeStyle = '#2005ba';
			g.strokeRect(posX,posY,w,h);
		}
		
        function drawText(g, text, height, posX, posY, w, h,color) {

			fillBox(g,posX-1,(posY-h),w,h,color);
            g.fillStyle = "black";
            g.font = height + "px Georgia";
            g.fillText(text, posX, posY);
        }

		var tempPos={};
		
		var j=Session.get("startY");
		var k=Session.get("startX");
		tempPos.startX=k;
		tempPos.startY=j;
		var alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		var colorString='#'+(Math.random()*0xFFFFFF<<0).toString(16);
		for(var i=0;i<studentsToPut.length;i++){
			drawText(g,studentsToPut[i].firstName,20,(1200/row)*(k),(j)*(800/col),1200/row,800/col,colorString);
			studentsToPut[i].seat=alphabet.charAt(k)+""+j;
			if (j==col)
			{

				k++;
				j=1;
			} else {
				j++;
			}
		}
		tempPos.endX=k;
		tempPos.endY=j;
		
		Session.set("startY",j);
		Session.set("startX",k);
		var id=(e.currentTarget.alt).substr(0,e.currentTarget.alt.indexOf(" "));
		console.log(id);
		Session.set("position",JSON.stringify(JSON.parse(Session.get("position")).concat([{"id":id,"pos":tempPos}])));
		Session.set("studentsComplete",JSON.stringify(JSON.parse(Session.get("studentsComplete")).concat(studentsToPut)));

		console.log((Session.get("position")));
		
 
    },

    'click .minus': function (e) {
        var $this = $(e.target);
        e.preventDefault();
        $this.attr('src', "/images/addButton.png");
        $this.removeClass("minus");
        $this.addClass("add");

        canvas = document.getElementById('floorPlan');
        g = canvas.getContext('2d');
		
		
		var venue=JSON.parse(Session.get("venueInfo"));
		var capacity = parseInt(venue.capacity);
		var col = parseInt(venue.column);
		var row = parseInt(venue.row);
		
		function erase(g, posX,posY, w, h){
			g.fillStyle = '#ecf7ff';
			g.fillRect(posX,posY,w,h);
			g.strokeStyle = '#2005ba';
			g.strokeRect(posX,posY,w,h);
		}
		// erase(g,0,0,1200/row,800/col);
		function isValid(a,b){
			if (a.id==b){
				return true;
			} else {return false;}
			
			
		}
		
		var thingy=e.currentTarget.alt.substr(0,e.currentTarget.alt.indexOf(" "));
		console.log(thingy);
		var posObj="";
		var posObj=JSON.parse(Session.get("position")).filter(function(a){
			if (a.id==thingy){
				
				return true;
			} else{
				return false;
			}
		});
		console.log(posObj[0]);
		var x=parseInt(posObj[0].pos.startX);
		var y=parseInt(posObj[0].pos.startY);
		
		while ((x!=posObj[0].pos.endX )|| (y!=posObj[0].pos.endY )){
			console.log(x+" "+y);
			erase(g,(1200/row)*x,(800/col)*(y-1),1200/row,800/col)
			if (y==col){
				x++;
				y=1;
				 
			} else {
				y++;
			}
		}
		var tempIndex;
		var temp=JSON.parse(Session.get("position"));
		for(var i=0;i<temp.length;i++){
			if (temp[i].id == posObj[0].id){
				tempIndex=i;
				break;
			}
			
		}
		console.log("i: "+tempIndex);
		var deleted=temp.splice(tempIndex,1);
		
		
		Session.set("position",JSON.stringify(temp));
		
		console.log(JSON.parse(Session.get("students")));
		var toMove=prompt("Do you want to shift the existing groups backwards?");
		
		if (toMove==1){
			//shift
			alert("shift "+((parseInt(posObj[0].pos.endX)-parseInt(posObj[0].pos.startX)+1)*parseInt(row)-(row-parseInt(posObj[0].pos.endY))-parseInt(posObj[0].pos.startY)));
		} else{
			
			alert ("pointer is now at the end of existing student on the map");
		}
		
		console.log("thingy"+deleted);
		console.log(x);
		
		
		
	
    },

    'click #groupName': function (e) {
        e.preventDefault();

        if ($(e.currentTarget).next().css('display') == 'none') {
            $(e.currentTarget).next().css('display', 'block');
        }
        else {
            $(e.currentTarget).next().css('display', 'none');
        }
    },

    'click .more': function (e) {
        var $this = $(e.target);
        e.preventDefault();
        //should trigger by group id or module id value
        //eq(i) position
        if ($(".group").eq(e.currentTarget.alt).css('display') == 'none')
            $(".group").eq(e.currentTarget.alt).css('display', 'block');
        else
            $(".group").eq(e.currentTarget.alt).css('display', 'none');
    },

    'click #done': function () {
        Router.go('/newPlan/chooseVenue/plan/savePlan');
    },

    'click #clear': function (e) {
        $("#autoFill").attr('disabled', false);
        $("#addRemove img").removeClass("minus");
        $("#addRemove img").addClass("add");
        $("#addRemove img").attr('src', "/images/addButton.png");
        var students = JSON.parse(Session.get("students"));
        var $this = $(e.target);
        e.preventDefault();
        canvas = document.getElementById('floorPlan');
        g = canvas.getContext('2d');
        g.fillStyle = '#ecf7ff';
        g.fillRect(0, 0, 1200, 800);

        // var capacity = 250;
        // var col = 25;
        // var row = 10;
		


        var venue = JSON.parse(Session.get("venueInfo"));
        //console.log("thigy haha"+ venue.row);
        var capacity = parseInt(venue.capacity);
        var col = parseInt(venue.column);
        var row = parseInt(venue.row);


        function drawRect(g, capacity, w, h) {
            /* Draw on the canvas */
            g.strokeStyle = "#2005ba";
            g.strokeRect(0, 0, w, h);
        }

		var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		var k=0;
        for (var j = 0; j <= col; j++) {
			g.fillStyle="black";
			g.font=20 + "px Georgia";
			g.fillText(j,0,(j * (800 / col))-10);

            for (var i = 1; i <= row; i++) {
				// if (i==1){
					g.fillStyle="black";
					g.font=20 + "px Georgia";
					g.fillText(alphabet[k],((1200 / row) * (k))+20,20);
					k++;
				// }
                drawRect(context, capacity, ((1200 / row) * i), j * (800 / col));
                // console.log("done");
            }
        }


        Session.set("studentsComplete", "[]");

        Session.set("startY", 1);
        Session.set("startX", 0);


    },


    'click #autoFill': function (e) {
        // var students = [];


        var students = JSON.parse(Session.get("students"));
        $("#autoFill").attr('disabled', true);

        var flattened = [];
        var studentsToPut = [];
        var t = [];
        for (var i = 0; i < students.length; i++) {

            t[i] = (students[i].course).reduce(function (a, b) {
                return a.concat(b.students);
            }, []);
        }
        flattened = t.reduce(function (a, b) {
            return a.concat(b);
        }, []);


        console.log(flattened.length);

        var $this = $(e.target);
        e.preventDefault();
        canvas = document.getElementById('floorPlan');
        g = canvas.getContext('2d');

        // var capacity = 250;
        // var col = 25;
        // var row = 10;
        var venue = JSON.parse(Session.get("venueInfo"));

        var capacity = parseInt(venue.capacity);
        var col = parseInt(venue.column);
        var row = parseInt(venue.row);

        function drawRect(g, posX, posY, w, h, color) {
            /* Draw on the canvas */
            g.fillStyle = color;
            g.fillRect(posX, posY, w, h);
        }


        function drawText(g, text, height, posX, posY, w, h) {
            // drawRect(g,posX,posY-h,w,h,"red");
            g.fillStyle = "black";
            g.font = height + "px Georgia";
            g.fillText(text, posX, posY);
        }


        // redraw line between them here

        if ((Session.get("startX") != 0) && (Session.get("startY") != 1)) {
            alert("you are not allowed to perform this action");
            //$("#autoFill").attr('disabled', true);
        } else {
            var j = 1;
            var k = 0;
            var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for (var i = 0; i < flattened.length; i++) {
                drawText(g, flattened[i].firstName, 20, (1200 / row) * (k), (j) * (800 / col), 1200 / row, 800 / col);
                flattened[i].seat = alphabet.charAt(k) + "" + j;
                if (j == col) {
                    // console.log("huh"+flattened[i].firstName);

                    k++;
                    j = 1;
                } else {
                    j++;
                }

            }
            Session.set("placed", Session.get("modules"));
            Session.set("studentsComplete", JSON.stringify(flattened));

        }


    }


});

Template.plan.helpers({

    goHome: function () {
        Router.go("/");
    },

    student: function () {
        return "a";

    },

    info: function () {

        var year = Session.get("year");
        var month = Session.get("month");
        var date = Session.get("day");
        var venueId = Session.get("venueId");
        var hour = Session.get("hour");
        var minute = Session.get("minute")


        var venue = VenueList.find().fetch();
        var venueName = "";
        for (var i = 0; i < venue.length; i++) {
            if (venue[i]._id == Session.get("venueID")) {
                venueName = venue[i].name;
                // console.log(venueName);
                break;
            }


        }


        var thingy = {};
        thingy.year = year;
        thingy.month = month;
        thingy.date = date;
        thingy.venueId = venueId;
        thingy.hour = hour;
        thingy.minute = minute;
        thingy.venueName = venueName;
        // console.log(thingy);
        return thingy;

    },


    data: function () {
		
		// console.log("thingy: "+VenueList.find({}).fetch());
		var venues = VenueList.findOne({"_id":Session.get("venueID")});
		// console.log("thingy: "+venues);

		Session.set("venueInfo",JSON.stringify(venues));
		Session.set("startX",0);
		Session.set("startY",1);
 
		// console.log("haha "+Session.get("venueInfo"));
		
		Session.set("position","[]");
		Session.set("studentsComplete","[]");
		

        var result = [];
        var modules = Session.get("modules").substr(2, Session.get("modules").length - 4).split("\",\"");
        var coursesByModule = modules.map(function (a) {

            var temp = CourseList.find().fetch();
            var tempRes = [];
            var tempStudent;
            // var tempToPush;

            for (var i = 0; i < temp.length; i++) {
                var tempM = temp[i].modules;

                for (var j = 0; j < tempM.length; j++) {
                    if (tempM[j].modulesCode == a) {

                        // console.log(StudentList.find({"programme.courseID":temp[i].courseID}).fetch().length);
                        tempStudent = StudentList.find({"programme.courseID": temp[i].courseID}).fetch();
                        // tempToPush.prog=temp[i];
                        // tempToPush.students=tempStudent;
                        tempRes.push({"prog": temp[i], "students": tempStudent});
                        // alert("a");
                    }
                }

            }

            return tempRes;
        });

        // var students=StudentList.find();

        for (var i = 0; i < modules.length; i++) {

            // console.log("thingy: " + ModuleList.findOne({modulesCode: modules[i]}).modulesName);

            modules[i] = modules[i] + ": " + ModuleList.findOne({modulesCode: modules[i]}).modulesName;

        }
        // console.log(coursesByModule);
        // alert ("a");
		var colors=["#e6fff9","#ccfff2","#b3ffec","#99ffe6","#80ffdf","#66ffd9","#4dffd2","#33ffcc","#1affc6","#00ffbf","#00e6ac","#00cc99","#00b386","#009973","#008060","#00664d","#004d39"];
        for (var i = 0; i < modules.length; i++) {
            result[i] = {
                "id": i,
                "moduleID": modules[i].substr(0, modules[i].indexOf(":")),
                "module": modules[i],
                "course": coursesByModule[i],
				"color": colors[i]
            };
            //	console.log(result[i].course.length);

        }
        Session.set("students", JSON.stringify(result));
        // console.log(result);
        return result;
    },


    info: function () {
        var year = Session.get("year");
        var month = Session.get("month");
        var date = Session.get("day");
        var venueId = Session.get("venueId");
        var hour = Session.get("hour");
        var minute = Session.get("minute")

        var venue = VenueList.find().fetch();
        var venueName = "";
        for (var i = 0; i < venue.length; i++) {
            if (venue[i]._id == Session.get("venueID")) {
                venueName = venue[i].name;
                //  console.log(venueName);
                break;
            }


        }


        var thingy = {};
        thingy.year = year;
        thingy.month = month;
        thingy.date = date;
        thingy.venueId = venueId;
        thingy.hour = hour;
        thingy.minute = minute;
        thingy.venueName = venueName;
        // console.log(thingy);
        return thingy;

    }


});

Template.plan.rendered = (function () {
    $('[data-toggle="tooltip"]').tooltip();
    
    canvas = document.getElementById('floorPlan');
    context = canvas.getContext('2d');
    context.fillStyle = '#ecf7ff';
    context.fillRect(0, 0, 1200, 800);


    // console.log("hahaha"+Session.get("venueInfo"));

    var venue = JSON.parse(Session.get("venueInfo"));
    // console.log("thigy haha"+ venue.row);
    var capacity = parseInt(venue.capacity);
    var col = parseInt(venue.column);
    var row = parseInt(venue.row);

	var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	var k=0;
	
    function drawRect(g, capacity, w, h) {
        /* Draw on the canvas */
        g.strokeStyle = "#2005ba";
        g.strokeRect(0, 0, w, h);
    }
	

    for (var j = 0; j <= col; j++) {
        // g.drawText(j+1,0,j * (800 / col));
		context.fillStyle="black";
		context.font=20 + "px Georgia";
		context.fillText(j,0,(j * (800 / col))-10);
		
		for (var i = 1; i <= row; i++) {
				
			context.fillStyle="black";
			context.font=20 + "px Georgia";
			context.fillText(alphabet[k],((1200 / row) * (k))+20,20);
			k++;
		
			console.log("there");
			drawRect(context, capacity, ((1200 / row) * i), j * (800 / col));
			
		}
    }

	
	
});

