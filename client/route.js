Router.configure({
    layoutTemplate: "login",
    notFoundTemplate: "notFound"
});


//newPlan
Router.route("/newPlan", function () {
    this.render("newPlan");
});

Router.route("/myPlan", function () {
    this.render("myPlan");
});

Router.route("/newPlan/chooseVenue/", function () {
    this.render("chooseVenue");

});
Router.route("/myPlans/plan", function () {
    this.layout("plan");
});

Router.route("/newPlan/chooseVenue/plan", function () {
    var venue = Session.get("venueID");
    var venueObj = VenueList.find({"_id": venue});
    console.log(venueObj);
    this.layout("plan", {
        to: 'content',
        data: function () {
            return venueObj;
        }
    });

});

Router.route("/newPlan/chooseVenue/plan/savePlan", function () {

    this.render("savePlan");

});

Router.route("/myPlan/preview", function () {
    this.render("preview");
});

//setting
Router.route("/setting", function () {
    this.render("setting");
});
